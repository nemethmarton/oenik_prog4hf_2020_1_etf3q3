﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace UnixStock.Wpf
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;
		private ProductVM selectedProduct;
		private ObservableCollection<ProductVM> allProducts;

		public ObservableCollection<ProductVM> AllProducts
		{
			get { return allProducts; }
			set { Set(ref allProducts, value); }
		}


		public ProductVM SelectedProduct
		{
			get { return selectedProduct; }
			set { Set(ref selectedProduct, value); }
		}

		public ICommand AddCmd { get; private set; }
		public ICommand DelCmd { get; private set; }
		public ICommand ModCmd { get; private set; }
		public ICommand LoadCmd { get; private set; }

		public Func<ProductVM, bool> EditorFunc { get; set; }

		public MainVM()
		{
			logic = new MainLogic();
			DelCmd = new RelayCommand(() => logic.ApiDelProduct(selectedProduct));
			AddCmd = new RelayCommand(() => logic.EditProduct(null, EditorFunc));
			ModCmd = new RelayCommand(() => logic.EditProduct(selectedProduct, EditorFunc));
			LoadCmd = new RelayCommand(() => AllProducts = new ObservableCollection<ProductVM>(logic.ApiGetProducts()));
		}
	}
}
