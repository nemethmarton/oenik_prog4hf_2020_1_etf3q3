﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace UnixStock.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:60624/api/ProductsApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed succesfully" : "Operation failed";
            Messenger.Default.Send(msg, "ProductResult");
        }

        public List<ProductVM> ApiGetProducts()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<ProductVM>>(json);
            return list;
        }

        public void ApiDelProduct(ProductVM product)
        {
            bool success = false;
            if (product != null)
            {
                string json = client.GetStringAsync(url + "del/" + product.Id).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationSuccess"];
            }
            SendMessage(success);
        }

        bool ApiEditProduct(ProductVM product, bool isEditing)
        {
            if (product == null) return false;
            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing) postData.Add(nameof(ProductVM.Id), product.Id.ToString());
            postData.Add(nameof(ProductVM.Id), product.Id.ToString());
            postData.Add(nameof(ProductVM.Name), product.Name);
            postData.Add(nameof(ProductVM.Category), product.Category);
            postData.Add(nameof(ProductVM.Price), product.Price.ToString());

            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

        public void EditProduct(ProductVM product, Func<ProductVM, bool> editor)
        {
            ProductVM clone = new ProductVM();
            if (product != null) clone.CopyFrom(product);
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (product != null) ApiEditProduct(clone, true);
                else success = ApiEditProduct(clone, false);
            }
            SendMessage(success == true);
        }
    }
}
