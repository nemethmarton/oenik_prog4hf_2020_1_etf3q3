﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnixStock.Wpf
{
    class ProductVM : ObservableObject
    {
		private int id;
		private string name;
		private string category;
		private int price;

		public int Price
		{
			get { return price; }
			set { Set(ref price, value); }
		}


		public string Category
		{
			get { return category; }
			set { Set(ref category, value); }
		}


		public string Name
		{
			get { return name; }
			set { Set(ref name, value); }
		}


		public int Id
		{
			get { return id; }
			set { Set(ref id, value); }
		}

		public void CopyFrom(ProductVM other)
		{
			if (other == null) return;
			this.Id = other.Id;
			this.Name = other.Name;
			this.Category = other.Category;
			this.Price = other.Price;
		}
	}
}
