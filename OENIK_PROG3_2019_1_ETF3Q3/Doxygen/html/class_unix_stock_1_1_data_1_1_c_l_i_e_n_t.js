var class_unix_stock_1_1_data_1_1_c_l_i_e_n_t =
[
    [ "CLIENT", "class_unix_stock_1_1_data_1_1_c_l_i_e_n_t.html#a9172ee3eb6559c46c154a43edec09c97", null ],
    [ "BILLING_ADDRESS", "class_unix_stock_1_1_data_1_1_c_l_i_e_n_t.html#a0cf321238895ee5ab4b2fc901c023fa3", null ],
    [ "CATEGORY", "class_unix_stock_1_1_data_1_1_c_l_i_e_n_t.html#a8945bcec4b227b28f5111e3c69eedd6a", null ],
    [ "CLIENTID", "class_unix_stock_1_1_data_1_1_c_l_i_e_n_t.html#a603363adaae8654f1b5c0d0795cd8185", null ],
    [ "COMMENT", "class_unix_stock_1_1_data_1_1_c_l_i_e_n_t.html#a895ef2e6076623e9f314644b1b8332e1", null ],
    [ "DISCOUNT", "class_unix_stock_1_1_data_1_1_c_l_i_e_n_t.html#a394f923eb90dbbb912bfec6422d7f2ac", null ],
    [ "NAME", "class_unix_stock_1_1_data_1_1_c_l_i_e_n_t.html#ab231008b8453ff421e05e68d827b2662", null ],
    [ "PHONE", "class_unix_stock_1_1_data_1_1_c_l_i_e_n_t.html#aa27ed1fa10c2ad17d6e6dc07a990d15b", null ],
    [ "SALE", "class_unix_stock_1_1_data_1_1_c_l_i_e_n_t.html#ad567c1de1b866044710b25fe92facfd5", null ],
    [ "SHIPPING_ADDRESS", "class_unix_stock_1_1_data_1_1_c_l_i_e_n_t.html#a32410d2debd96e4b2cc1d63c13b57c62", null ]
];