var class_unix_stock_1_1_data_1_1_d_e_p_t =
[
    [ "DEPT", "class_unix_stock_1_1_data_1_1_d_e_p_t.html#aaf5978c07ef46645881c6736e68816b8", null ],
    [ "ADDRESS", "class_unix_stock_1_1_data_1_1_d_e_p_t.html#acbaab604453e7da5081df5e2b5bdeeb3", null ],
    [ "CITY", "class_unix_stock_1_1_data_1_1_d_e_p_t.html#aa9b57fd62270bae97adf256d74876184", null ],
    [ "COUNTY", "class_unix_stock_1_1_data_1_1_d_e_p_t.html#a4b1103b6e65901506abba22222234005", null ],
    [ "DEPTID", "class_unix_stock_1_1_data_1_1_d_e_p_t.html#a72ffb5cd0ee793c4797b75634b1dc6cc", null ],
    [ "PHONE", "class_unix_stock_1_1_data_1_1_d_e_p_t.html#aa066b6365aaf6a12e8ece7aaa63a8c75", null ],
    [ "SALE", "class_unix_stock_1_1_data_1_1_d_e_p_t.html#a1d4c52363fd3b9a72cf7173b3f6d62a9", null ],
    [ "STOCK", "class_unix_stock_1_1_data_1_1_d_e_p_t.html#ac35c5f7948a2d5051895608e5bf24b5c", null ],
    [ "ZIP", "class_unix_stock_1_1_data_1_1_d_e_p_t.html#a2726c38192feafc1e2c89f2ba5401264", null ]
];