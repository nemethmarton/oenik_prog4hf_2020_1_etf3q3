var class_unix_stock_1_1_logic_1_1_logic =
[
    [ "Logic", "class_unix_stock_1_1_logic_1_1_logic.html#a5c351610248872188ceb405421d3f3ae", null ],
    [ "Logic", "class_unix_stock_1_1_logic_1_1_logic.html#abcb2e1de357707468c5c413bf28562b7", null ],
    [ "Bills", "class_unix_stock_1_1_logic_1_1_logic.html#a3b3c6d384719fdd65e2c9f4d58b50cbf", null ],
    [ "ClientCreate", "class_unix_stock_1_1_logic_1_1_logic.html#a003f16e7d41c513fe65e331c008f157d", null ],
    [ "ClientDelete", "class_unix_stock_1_1_logic_1_1_logic.html#a214274ec6fc05cb04788d733f7348424", null ],
    [ "ClientList", "class_unix_stock_1_1_logic_1_1_logic.html#a1ee955c97772c7020ff2569439ea7850", null ],
    [ "ClientModify", "class_unix_stock_1_1_logic_1_1_logic.html#aa06cad18160ad28f655a5ae7931391a6", null ],
    [ "ClientsByDiscount", "class_unix_stock_1_1_logic_1_1_logic.html#a2b2615628d0b0e0a2d4ee45673472ba2", null ],
    [ "DeptCreate", "class_unix_stock_1_1_logic_1_1_logic.html#a90ef764047f1f4ab666a71201545f2f9", null ],
    [ "DeptDelete", "class_unix_stock_1_1_logic_1_1_logic.html#adb1eb0670ca858bbc36609c03834e5e7", null ],
    [ "DeptList", "class_unix_stock_1_1_logic_1_1_logic.html#a9bd4c2dc015cd412eeb50febab9ae9a1", null ],
    [ "DeptModify", "class_unix_stock_1_1_logic_1_1_logic.html#ab492903b441681680be5315c2831d7ec", null ],
    [ "FakeClients", "class_unix_stock_1_1_logic_1_1_logic.html#a8aa17ba94b4297f56c978045dcf2c25d", null ],
    [ "JavaEndpoint", "class_unix_stock_1_1_logic_1_1_logic.html#afc4cb79f3b1d9404853ff7475fb42c2b", null ],
    [ "ProductCreate", "class_unix_stock_1_1_logic_1_1_logic.html#ae42654da3cbfc3840ea88138130bd908", null ],
    [ "ProductDelete", "class_unix_stock_1_1_logic_1_1_logic.html#ab7c8a519f9f25de9d0c38b50183c1963", null ],
    [ "ProductList", "class_unix_stock_1_1_logic_1_1_logic.html#a9b7be74d5526e1d82eefd16e29eb6122", null ],
    [ "ProductModify", "class_unix_stock_1_1_logic_1_1_logic.html#a76819fd77934ca96e945985435ac71f3", null ],
    [ "StockByDept", "class_unix_stock_1_1_logic_1_1_logic.html#a5256f7dc939ac9e6e8abb54f3831e9bb", null ]
];