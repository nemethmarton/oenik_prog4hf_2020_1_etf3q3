var hierarchy =
[
    [ "UnixStock.Data.CLIENT", "class_unix_stock_1_1_data_1_1_c_l_i_e_n_t.html", null ],
    [ "DbContext", null, [
      [ "UnixStock.Data.Entities", "class_unix_stock_1_1_data_1_1_entities.html", null ]
    ] ],
    [ "UnixStock.Data.DEPT", "class_unix_stock_1_1_data_1_1_d_e_p_t.html", null ],
    [ "UnixStock.Repository.IRepository< T >", "interface_unix_stock_1_1_repository_1_1_i_repository.html", [
      [ "UnixStock.Repository.Repository< T >", "class_unix_stock_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "UnixStock.Repository.IRepository< CLIENT >", "interface_unix_stock_1_1_repository_1_1_i_repository.html", null ],
    [ "UnixStock.Repository.IRepository< DEPT >", "interface_unix_stock_1_1_repository_1_1_i_repository.html", null ],
    [ "UnixStock.Repository.IRepository< PRODUCT >", "interface_unix_stock_1_1_repository_1_1_i_repository.html", null ],
    [ "UnixStock.Repository.IRepository< SALE >", "interface_unix_stock_1_1_repository_1_1_i_repository.html", null ],
    [ "UnixStock.Repository.IRepository< SALE_HAS_PRODUCT >", "interface_unix_stock_1_1_repository_1_1_i_repository.html", null ],
    [ "UnixStock.Repository.IRepository< STOCK >", "interface_unix_stock_1_1_repository_1_1_i_repository.html", null ],
    [ "UnixStock.Logic.Logic", "class_unix_stock_1_1_logic_1_1_logic.html", null ],
    [ "UnixStock.Data.PRODUCT", "class_unix_stock_1_1_data_1_1_p_r_o_d_u_c_t.html", null ],
    [ "UnixStock.Program.Program", "class_unix_stock_1_1_program_1_1_program.html", null ],
    [ "UnixStock.Data.SALE", "class_unix_stock_1_1_data_1_1_s_a_l_e.html", null ],
    [ "UnixStock.Data.SALE_HAS_PRODUCT", "class_unix_stock_1_1_data_1_1_s_a_l_e___h_a_s___p_r_o_d_u_c_t.html", null ],
    [ "UnixStock.Data.STOCK", "class_unix_stock_1_1_data_1_1_s_t_o_c_k.html", null ],
    [ "UnixStock.Logic.Tests.Tests", "class_unix_stock_1_1_logic_1_1_tests_1_1_tests.html", null ]
];