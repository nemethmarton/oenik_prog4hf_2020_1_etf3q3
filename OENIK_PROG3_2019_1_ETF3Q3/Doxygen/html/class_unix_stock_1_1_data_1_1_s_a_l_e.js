var class_unix_stock_1_1_data_1_1_s_a_l_e =
[
    [ "SALE", "class_unix_stock_1_1_data_1_1_s_a_l_e.html#a7e1dc8c3cb4b91b6bc956b4910fd1337", null ],
    [ "CLIENT", "class_unix_stock_1_1_data_1_1_s_a_l_e.html#ad29c0cfb02426a589c0f3c99cfe51369", null ],
    [ "CLIENTID", "class_unix_stock_1_1_data_1_1_s_a_l_e.html#a8cbbdcd2c33b721ea24858e021c39076", null ],
    [ "DEPT", "class_unix_stock_1_1_data_1_1_s_a_l_e.html#a30d96ada102c4c79d7e53d87b00acd4c", null ],
    [ "DEPTID", "class_unix_stock_1_1_data_1_1_s_a_l_e.html#a627249e8fc83ff2f8089cddbbc9dc08b", null ],
    [ "SALE_HAS_PRODUCT", "class_unix_stock_1_1_data_1_1_s_a_l_e.html#a16f5cee8e0080f2cee3e5b0414b5a564", null ],
    [ "SALEDATE", "class_unix_stock_1_1_data_1_1_s_a_l_e.html#a3a9d007891860dfb66d6912e3e7e470c", null ],
    [ "SALEID", "class_unix_stock_1_1_data_1_1_s_a_l_e.html#a1715b8961b0b105074ace608735c1bef", null ]
];