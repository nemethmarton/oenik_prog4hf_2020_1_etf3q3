var namespace_unix_stock_1_1_data =
[
    [ "CLIENT", "class_unix_stock_1_1_data_1_1_c_l_i_e_n_t.html", "class_unix_stock_1_1_data_1_1_c_l_i_e_n_t" ],
    [ "DEPT", "class_unix_stock_1_1_data_1_1_d_e_p_t.html", "class_unix_stock_1_1_data_1_1_d_e_p_t" ],
    [ "Entities", "class_unix_stock_1_1_data_1_1_entities.html", "class_unix_stock_1_1_data_1_1_entities" ],
    [ "PRODUCT", "class_unix_stock_1_1_data_1_1_p_r_o_d_u_c_t.html", "class_unix_stock_1_1_data_1_1_p_r_o_d_u_c_t" ],
    [ "SALE", "class_unix_stock_1_1_data_1_1_s_a_l_e.html", "class_unix_stock_1_1_data_1_1_s_a_l_e" ],
    [ "SALE_HAS_PRODUCT", "class_unix_stock_1_1_data_1_1_s_a_l_e___h_a_s___p_r_o_d_u_c_t.html", "class_unix_stock_1_1_data_1_1_s_a_l_e___h_a_s___p_r_o_d_u_c_t" ],
    [ "STOCK", "class_unix_stock_1_1_data_1_1_s_t_o_c_k.html", "class_unix_stock_1_1_data_1_1_s_t_o_c_k" ]
];