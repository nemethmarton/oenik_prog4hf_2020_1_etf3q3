var class_unix_stock_1_1_repository_1_1_repository =
[
    [ "Repository", "class_unix_stock_1_1_repository_1_1_repository.html#a8f48a1344e5ae4c034591cbf788361c7", null ],
    [ "Create", "class_unix_stock_1_1_repository_1_1_repository.html#a840e32a1af7ce90409aee794c0ddb9a0", null ],
    [ "Delete", "class_unix_stock_1_1_repository_1_1_repository.html#a47b5940c6043a394e99d581aa18b3549", null ],
    [ "Delete", "class_unix_stock_1_1_repository_1_1_repository.html#a1b9d60098b4ca5347cef5ea809d2d49d", null ],
    [ "Read", "class_unix_stock_1_1_repository_1_1_repository.html#a04c448f843c3bf89a46a7a78f2990b7f", null ],
    [ "ReadAll", "class_unix_stock_1_1_repository_1_1_repository.html#a8401b979ec16c221e604be380b6dba11", null ],
    [ "Update", "class_unix_stock_1_1_repository_1_1_repository.html#a56c6a2ca6c7f486822b3e1270136b2a5", null ]
];