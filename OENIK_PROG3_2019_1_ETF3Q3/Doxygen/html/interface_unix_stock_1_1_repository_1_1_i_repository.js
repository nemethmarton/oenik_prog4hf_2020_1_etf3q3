var interface_unix_stock_1_1_repository_1_1_i_repository =
[
    [ "Create", "interface_unix_stock_1_1_repository_1_1_i_repository.html#a8cf73aba429b7a15d9ed8eec1dbbf9bd", null ],
    [ "Delete", "interface_unix_stock_1_1_repository_1_1_i_repository.html#a239d3279250df1a9d6afe05b11e8b5f0", null ],
    [ "Delete", "interface_unix_stock_1_1_repository_1_1_i_repository.html#a85ca013b49c7742f3e755631d1e25147", null ],
    [ "Read", "interface_unix_stock_1_1_repository_1_1_i_repository.html#ac3692c0d198f0569904bb7ee975cc6e4", null ],
    [ "ReadAll", "interface_unix_stock_1_1_repository_1_1_i_repository.html#a4f426b14f965d51dc13e6f8684891ab5", null ],
    [ "Update", "interface_unix_stock_1_1_repository_1_1_i_repository.html#ae7d59a3ee61af57f07bf7c0764dccb10", null ]
];