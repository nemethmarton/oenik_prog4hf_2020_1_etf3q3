var class_unix_stock_1_1_data_1_1_p_r_o_d_u_c_t =
[
    [ "PRODUCT", "class_unix_stock_1_1_data_1_1_p_r_o_d_u_c_t.html#adac1853e9f531f645806d3081be1d63d", null ],
    [ "CATEGORY", "class_unix_stock_1_1_data_1_1_p_r_o_d_u_c_t.html#ace05e83ef36ac11445d0b2311c937196", null ],
    [ "NAME", "class_unix_stock_1_1_data_1_1_p_r_o_d_u_c_t.html#a4b285ca805e4a66566c104dadbd44be6", null ],
    [ "PRICE", "class_unix_stock_1_1_data_1_1_p_r_o_d_u_c_t.html#a27ccad32d595fc21e22022c2f6175888", null ],
    [ "PRODUCTID", "class_unix_stock_1_1_data_1_1_p_r_o_d_u_c_t.html#af3728b378a0bbbda93b394508af4ba94", null ],
    [ "SALE_HAS_PRODUCT", "class_unix_stock_1_1_data_1_1_p_r_o_d_u_c_t.html#a8beaa23b32d66875045d12846b182269", null ],
    [ "SIZE", "class_unix_stock_1_1_data_1_1_p_r_o_d_u_c_t.html#a54908f5323980ee146acb041351dda17", null ],
    [ "STOCK", "class_unix_stock_1_1_data_1_1_p_r_o_d_u_c_t.html#a7d934b3b425b7124dc9a90d8097af3ad", null ],
    [ "WEIGHT", "class_unix_stock_1_1_data_1_1_p_r_o_d_u_c_t.html#ac1f0d5c21ae0ee9f2ea5f282a2569eaa", null ]
];