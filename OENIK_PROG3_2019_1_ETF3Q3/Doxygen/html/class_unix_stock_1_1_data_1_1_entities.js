var class_unix_stock_1_1_data_1_1_entities =
[
    [ "Entities", "class_unix_stock_1_1_data_1_1_entities.html#a5da505e2d4f544d0dffaf9fa6b678c9a", null ],
    [ "OnModelCreating", "class_unix_stock_1_1_data_1_1_entities.html#adf39d1a5dd103b2af4e8dd3169d755eb", null ],
    [ "CLIENT", "class_unix_stock_1_1_data_1_1_entities.html#af6a555b613341e60ee3f47f9e0cd5269", null ],
    [ "DEPT", "class_unix_stock_1_1_data_1_1_entities.html#a585356edf711ff1f5d1a30974e3c4535", null ],
    [ "PRODUCT", "class_unix_stock_1_1_data_1_1_entities.html#a67216dca4487cf87b839ad5a59c21e65", null ],
    [ "SALE", "class_unix_stock_1_1_data_1_1_entities.html#a0fcad60ecd378cb1267b860e7befee61", null ],
    [ "SALE_HAS_PRODUCT", "class_unix_stock_1_1_data_1_1_entities.html#a640c30d119a4e23fc2aaad9a7bb37e27", null ],
    [ "STOCK", "class_unix_stock_1_1_data_1_1_entities.html#acc08e5ee1738364fb2890393d9157afd", null ]
];