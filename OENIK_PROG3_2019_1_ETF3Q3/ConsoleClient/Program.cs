﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace UnixStock.ConsoleClient
{
    class Program
    {
        public class Product
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Category { get; set; }
            public string Size { get; set; }
            public int Weight { get; set; }
            public int Price { get; set; }
            public override string ToString()
            {
                return $"Id={Id}\tName={Name}\tPrice={Price}";
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Waiting...");
            Console.ReadLine();

            string url = "http://localhost:60624/api/ProductsApi/";
            int productIdToDelete = 1;

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Product>>(json);
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }
                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Product.Name), "Porsche 911");
                postData.Add(nameof(Product.Price), "50000");
                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                response = client.GetStringAsync(url + "del/" + productIdToDelete).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

            }
        }
    }
}
