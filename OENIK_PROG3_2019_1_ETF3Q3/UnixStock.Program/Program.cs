﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnixStock.Logic;

namespace UnixStock.Program
{
    /// <summary>
    /// Console application
    /// </summary>
    class Program
    {
        /// <summary>
        /// Main class fro controlling the menu
        /// </summary>
        static void Main(string[] args)
        {
            Console.Title = "UnixStock";
            var dataDirectory = ConfigurationSettings.AppSettings["DataDirectory"];
            var absoluteDataDirectory = Path.GetFullPath(dataDirectory);
            AppDomain.CurrentDomain.SetData("DataDirectory", absoluteDataDirectory);

            Logic.Logic BL = new Logic.Logic();

            int userInput = 0;
            int id;
            string name;
            int price;

            do
            {
                DisplayMenu();
                userInput = Convert.ToInt32(Console.ReadLine());
                switch (userInput)
                {
                    case 1:
                        Writer(BL.ProductList());
                        break;
                    case 11:
                        id = Convert.ToInt32(Reader("Product ID"));
                        name = Reader("Product Name");
                        //BL.ProductCreate(id, name);
                        break;
                    case 12:
                        id = Convert.ToInt32(Reader("Product ID"));
                        price = Convert.ToInt32(Reader("Product Price"));
                        //BL.ProductModify(id, price);
                        break;
                    case 13:
                        id = Convert.ToInt32(Reader("Product ID"));
                        BL.ProductDelete(id);
                        break;
                    case 2:
                        Writer(BL.ClientList());
                        break;
                    case 21:
                        id = Convert.ToInt32(Reader("Client ID"));
                        name = Reader("Client Name");
                        BL.ClientCreate(id, name);
                        break;
                    case 22:
                        id = Convert.ToInt32(Reader("Client ID"));
                        price = Convert.ToInt32(Reader("Client Discount"));
                        BL.ClientModify(id, price);
                        break;
                    case 23:
                        id = Convert.ToInt32(Reader("Client ID"));
                        BL.ClientDelete(id);
                        break;
                    case 3:
                        Writer(BL.DeptList());
                        break;
                    case 31:
                        id = Convert.ToInt32(Reader("Dept ID"));
                        name = Reader("Dept Address");
                        BL.DeptCreate(id, name);
                        break;
                    case 32:
                        id = Convert.ToInt32(Reader("Dept ID"));
                        price = Convert.ToInt32(Reader("Dept Phone"));
                        BL.DeptModify(id, price);
                        break;
                    case 33:
                        id = Convert.ToInt32(Reader("Dept ID"));
                        BL.DeptDelete(id);
                        break;
                    case 4:
                        Writer(BL.FakeClients());
                        break;
                    case 41:
                        Writer(BL.Bills());
                        break;
                    case 42:
                        Writer(BL.StockByDept());
                        break;
                    case 43:
                        Writer(BL.ClientsByDiscount());
                        break;
                    case 44:
                        Writer(BL.JavaEndpoint());
                        break;
                }
            }
            while (userInput != 5);
        }

        /// <summary>
        /// Display the default menu
        /// </summary>
        static void DisplayMenu()
        {
            Console.Clear();
            Console.WriteLine("UnixStock Raktárkezelés");
            Console.WriteLine("\n1. Termékek listázása");
            Console.WriteLine("11. Termék létrehozása");
            Console.WriteLine("12. Termék módosítása");
            Console.WriteLine("13. Termék törlése");
            Console.WriteLine("\n2. Ügyfelek listázása");
            Console.WriteLine("21. Ügyfél létrehozása");
            Console.WriteLine("22. Ügyfél módosítása");
            Console.WriteLine("23. Ügyfél törlése");
            Console.WriteLine("\n3. Telephelyek listázása");
            Console.WriteLine("31. Telephely létrehozása");
            Console.WriteLine("32. Telephely módosítása");
            Console.WriteLine("33. Telephely törlése");
            Console.WriteLine("\n4. Kamu ügyfelek listázása");
            Console.WriteLine("41. Számlák listázása");
            Console.WriteLine("42. Árukészlet telephelyenként");
            Console.WriteLine("43. Ügyfelek csoportosítása kedvezmény szerint");
            Console.WriteLine("44. Adatlekérés Java végponttól");
            Console.WriteLine("\n5. Kilépés");
            Console.Write("\nAdja meg a menüpont számát:");
        }

        /// <summary>
        /// Writes out a specific method to the console
        /// </summary>
        /// <param name="input">given by the logic class</param>
        static void Writer(string input)
        {
            Console.Clear();
            Console.WriteLine(input);
            Console.ReadLine();
            DisplayMenu();
        }

        /// <summary>
        /// Reading data from console
        /// </summary>
        /// <param name="paramName">parameter the user need to enter</param>
        /// <returns>input from user</returns>
        static string Reader(string paramName)
        {
            Console.Clear();
            Console.Write("Adja meg a bemeneti paramétert {" + paramName + "} :");
            string param = Console.ReadLine();
            DisplayMenu();
            return param;
        }
    }
}
