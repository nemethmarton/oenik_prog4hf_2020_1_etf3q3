﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnixStock.Repository;
using UnixStock.Data;

namespace UnixStock.Logic
{
    /// <summary>
    /// Business Logic Class
    /// </summary>
    public class Logic
    {
        private IRepository<CLIENT> clientRepo;

        private IRepository<DEPT> deptRepo;

        private IRepository<PRODUCT> productRepo;

        private IRepository<SALE> saleRepo;

        private IRepository<SALE_HAS_PRODUCT> shpRepo;

        private IRepository<STOCK> stockRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// Constructor with default Repository
        /// </summary>
        public Logic()
        {
            this.clientRepo = new Repository<CLIENT>();
            this.deptRepo = new Repository<DEPT>();
            this.productRepo = new Repository<PRODUCT>();
            this.saleRepo = new Repository<SALE>();
            this.shpRepo = new Repository<SALE_HAS_PRODUCT>();
            this.stockRepo = new Repository<STOCK>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// Constructor for mock repo
        /// </summary>
        /// <param name="clientRepo">Repository for Clients</param>
        /// <param name="deptRepo">Repository for Departments</param>
        /// <param name="productRepo">Repository for Products</param>
        /// <param name="saleRepo">Repository for Sales</param>
        /// <param name="shpRepo">Repository for Sale_Has_Product</param>
        /// <param name="stockRepo">Repository for Stocks</param>
        public Logic(IRepository<CLIENT> clientRepo, IRepository<DEPT> deptRepo, IRepository<PRODUCT> productRepo, IRepository<SALE> saleRepo, IRepository<SALE_HAS_PRODUCT> shpRepo, IRepository<STOCK> stockRepo)
        {
            this.clientRepo = clientRepo;
            this.deptRepo = deptRepo;
            this.productRepo = productRepo;
            this.saleRepo = saleRepo;
            this.shpRepo = shpRepo;
            this.stockRepo = stockRepo;
        }

        /// <summary>
        /// List all product
        /// </summary>
        /// <returns>string: one product per line</returns>
        public string ProductList()
        {
            var q = productRepo.ReadAll().ToList();
            string value = "";
            foreach (var item in q)
            {
                value += item.CATEGORY + " " + item.NAME + " " + item.SIZE + " " + item.WEIGHT + " " + item.PRICE + "\n";
            }

            return value;
        }

        public IList<PRODUCT> GetAllProducts()
        {
            return productRepo.ReadAll().ToList();
        }

        public PRODUCT GetOneProduct(int id)
        {
            return productRepo.Read(id);
        }

        /// <summary>
        /// Create new Product
        /// </summary>
        /// <param name="id">Product ID</param>
        /// <param name="name">Product Name</param>
        public void ProductCreate(int id, string name, string Category, string Size, int Weight, int Price)
        {
            PRODUCT p = new PRODUCT();
            p.PRODUCTID = id;
            p.NAME = name;
            p.CATEGORY = Category;
            p.SIZE = Size;
            p.PRICE = Price;
            p.WEIGHT = Weight;
            productRepo.Create(p);
        }

        /// <summary>
        /// Modify a specific product
        /// </summary>
        /// <param name="id">Product ID</param>
        /// <param name="newPrice">New price for the product</param>
        public bool ProductModify(int id, string Name, string Category, string Size, int Weight, int newPrice)
        {
            PRODUCT mod = productRepo.Read(id);
            if (mod == null) return false;
            mod.NAME = Name;
            mod.CATEGORY = Category;
            mod.SIZE = Size;
            mod.PRICE = newPrice;
            mod.WEIGHT = Weight;
            productRepo.Update(mod, id);
            return true;
        }

        /// <summary>
        /// Delete a specifc product
        /// </summary>
        /// <param name="id">Product ID</param>
        public bool ProductDelete(int id)
        {
            return productRepo.Delete(id);
        }

        /// <summary>
        /// List all client
        /// </summary>
        /// <returns>string: one client per line</returns>
        public string ClientList()
        {
            var q = clientRepo.ReadAll().ToList();
            string value = "";
            foreach (var item in q)
            {
                value += item.NAME + " " + item.CATEGORY + " " + item.SHIPPING_ADDRESS + " " + item.PHONE + " " + item.DISCOUNT + " " + item.COMMENT + "\n";
            }

            return value;
        }

        /// <summary>
        /// Create new client
        /// </summary>
        /// <param name="id">Client ID</param>
        /// <param name="name">Client Name</param>
        public void ClientCreate(int id, string name)
        {
            CLIENT c = new CLIENT();
            c.CLIENTID = id;
            c.NAME = name;
            clientRepo.Create(c);
        }

        /// <summary>
        /// Modify a specific client
        /// </summary>
        /// <param name="id">Client ID</param>
        /// <param name="discount">New discount for the client</param>
        public void ClientModify(int id, int discount)
        {
            CLIENT mod = clientRepo.Read(id);
            mod.DISCOUNT = discount;
            clientRepo.Update(mod, id);
        }

        /// <summary>
        /// Delete a specifc Client
        /// </summary>
        /// <param name="id">Client ID</param>
        public void ClientDelete(int id)
        {
            clientRepo.Delete(id);
        }

        /// <summary>
        /// List all departments
        /// </summary>
        /// <returns>string: one dept per line</returns>
        public string DeptList()
        {
            var q = deptRepo.ReadAll();
            string value = "";
            foreach (var item in q)
            {
                value += $"{item.DEPTID} {item.COUNTY} {item.ZIP} {item.CITY} {item.ADDRESS} {item.PHONE}\n";
            }
            return value;
        }

        /// <summary>
        /// Create new department
        /// </summary>
        /// <param name="id">Dept ID</param>
        /// <param name="address">Dept address</param>
        public void DeptCreate(int id, string address)
        {
            DEPT d = new DEPT();
            d.DEPTID = id;
            d.ADDRESS = address;
            deptRepo.Create(d);
        }

        /// <summary>
        /// Modify a specific client
        /// </summary>
        /// <param name="id">Dept ID</param>
        /// <param name="phone">New phone number for the dept</param>
        public void DeptModify(int id, int phone)
        {
            DEPT mod = deptRepo.Read(id);
            mod.PHONE = phone;
            deptRepo.Update(mod, id);
        }

        /// <summary>
        /// Delete a specifc Department
        /// </summary>
        /// <param name="id">Department ID</param>
        public void DeptDelete(int id)
        {
            deptRepo.Delete(id);
        }

        /// <summary>
        /// Create bills from sale table
        /// </summary>
        /// <returns>string: one bill per line</returns>
        public string Bills()
        {
            var q = from x in saleRepo.ReadAll()
                    join y in shpRepo.ReadAll() on x.SALEID equals y.SALEID
                    join z in productRepo.ReadAll() on y.PRODUCTID equals z.PRODUCTID
                    join c in clientRepo.ReadAll() on x.CLIENTID equals c.CLIENTID
                    select new
                    {
                        Name = c.NAME,
                        Product = z.NAME,
                        Quantity = y.QUANTITY,
                        Price = y.QUANTITY * z.PRICE,
                        Date = x.SALEDATE
                    };
            string value = "";
            foreach (var item in q)
            {
                value = $"{item.Name} {item.Product} {item.Quantity} {item.Price} {item.Date}\n";
            }
            return value;
        }

        /// <summary>
        /// Returns the private clients with the same address as a company
        /// </summary>
        /// <returns>string: Company : Fake Private Client</returns>
        public string FakeClients()
        {
            string value = "";
            var q = from x in clientRepo.ReadAll()
                    join y in clientRepo.ReadAll() on x.SHIPPING_ADDRESS equals y.SHIPPING_ADDRESS
                    where x.NAME != y.NAME
                    where y.CATEGORY == "Private"
                    select new
                    {
                        Name = x.NAME,
                        Name2 = y.NAME
                    };
            foreach (var item in q)
            {
                value += $"{item.Name} = {item.Name2}\n";
            }
            return value;
        }

        /// <summary>
        /// Returns the current stock for every product at all departments
        /// </summary>
        /// <returns>string: dept address, product name, quantity</returns>
        public string StockByDept()
        {
            string value = "";
            var q = from x in stockRepo.ReadAll()
                    join y in deptRepo.ReadAll() on x.DEPTID equals y.DEPTID
                    join z in productRepo.ReadAll() on x.PRODUCTID equals z.PRODUCTID
                    select new
                    {
                        Dept = y.ADDRESS,
                        Product = z.NAME,
                        Quantity = x.QUANTITY
                    };
            foreach (var item in q)
            {
                value += $"{item.Dept} {item.Product} {item.Quantity}\n";
            }
            return value;
        }

        /// <summary>
        /// Retruns the number of clients with the same discount rate
        /// </summary>
        /// <returns>string discount%, number of client</returns>
        public string ClientsByDiscount()
        {
            string value = "";
            var q = from x in clientRepo.ReadAll()
                    group x by x.DISCOUNT into g
                    select new
                    {
                        Discount = g.Key,
                        Count = g.Count()
                    };
            foreach (var item in q)
            {
                value += $"{item.Discount}%  {item.Count}db\n";
            }
            return value;
        }

        /// <summary>
        /// Get data from Java application
        /// </summary>
        /// <returns>string</returns>
        public string JavaEndpoint()
        {
            return "GET/localhost:8080";
        }
    }
}
