//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UnixStock.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRODUCT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRODUCT()
        {
            this.SALE_HAS_PRODUCT = new HashSet<SALE_HAS_PRODUCT>();
            this.STOCK = new HashSet<STOCK>();
        }
    
        public decimal PRODUCTID { get; set; }
        public string NAME { get; set; }
        public string CATEGORY { get; set; }
        public string SIZE { get; set; }
        public Nullable<decimal> WEIGHT { get; set; }
        public Nullable<decimal> PRICE { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SALE_HAS_PRODUCT> SALE_HAS_PRODUCT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STOCK> STOCK { get; set; }
    }
}
