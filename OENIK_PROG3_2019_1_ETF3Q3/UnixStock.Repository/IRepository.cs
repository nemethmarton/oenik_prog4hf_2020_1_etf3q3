﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnixStock.Repository
{
    public interface IRepository<T> where T : class
    {
        void Create(T element);

        T Read(int id);

        IEnumerable<T> ReadAll();

        void Update(T newElement, int oldId);

        bool Delete(int id);

        void Delete(T elementToDelete);
    }
}
