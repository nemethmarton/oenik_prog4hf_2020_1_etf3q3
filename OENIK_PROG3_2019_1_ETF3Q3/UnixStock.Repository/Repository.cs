﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace UnixStock.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private Data.Entities context;

        private IDbSet<T> dbEntity;

        public Repository()
        {
            context = new Data.Entities();
            dbEntity = context.Set<T>();
        }

        public void Create(T element)
        {
            dbEntity.Add(element);
            context.SaveChanges();
        }

        public bool Delete(int id)
        {
            T element = dbEntity.Find(id);
            if (element == null) return false;
            dbEntity.Remove(element);
            context.SaveChanges();
            return true;
        }

        public void Delete(T elementToDelete)
        {
            dbEntity.Remove(elementToDelete);
            context.SaveChanges();
        }

        public T Read(int id)
        {
            return dbEntity.Find(id);
        }

        public IEnumerable<T> ReadAll()
        {
            return dbEntity;
        }

        public void Update(T newElement, int oldId)
        {
            Delete(oldId);
            Create(newElement);
        }
    }
}
