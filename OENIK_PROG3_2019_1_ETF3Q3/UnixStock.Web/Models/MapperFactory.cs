﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UnixStock.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UnixStock.Data.PRODUCT, UnixStock.Web.Models.Product>().
                    ForMember(dest => dest.Id, map => map.MapFrom(src => src.PRODUCTID)).
                    ForMember(dest => dest.Name, map => map.MapFrom(src => src.NAME)).
                    ForMember(dest => dest.Category, map => map.MapFrom(src => src.CATEGORY)).
                    ForMember(dest => dest.Size, map => map.MapFrom(src => src.SIZE)).
                    ForMember(dest => dest.Weight, map => map.MapFrom(src => src.WEIGHT)).
                    ForMember(dest => dest.Price, map => map.MapFrom(src => src.PRICE));

            });
            return config.CreateMapper();
        }
    }
}