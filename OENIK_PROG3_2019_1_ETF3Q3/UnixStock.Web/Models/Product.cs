﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UnixStock.Web.Models
{
    public class Product
    {
        [Display(Name ="Product Id")]
        [Required]
        public int Id { get; set; }

        [Display(Name = "Product Name")]
        [Required]
        [StringLength(100, MinimumLength = 10)]
        public string Name { get; set; }

        [Display(Name = "Product Category")]
        [Required]
        public string Category { get; set; }

        [Display(Name = "Product Size")]
        [Required]
        public string Size { get; set; }

        [Display(Name = "Product Weight")]
        [Required]
        public int Weight { get; set; }

        [Display(Name = "Product Price")]
        [Required]
        public int Price { get; set; }
    }
}