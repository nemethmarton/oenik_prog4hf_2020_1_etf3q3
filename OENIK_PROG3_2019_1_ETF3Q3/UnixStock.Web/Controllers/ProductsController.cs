﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UnixStock.Web.Models;

namespace UnixStock.Web.Controllers
{
    public class ProductsController : Controller
    {
        Logic.Logic logic;
        IMapper mapper;
        PrdocutsViewModel model;

        public ProductsController()
        {
            logic = new Logic.Logic();
            mapper = MapperFactory.CreateMapper();
            model = new PrdocutsViewModel();
            model.EditedProduct = new Product();

            var products = logic.GetAllProducts();
            model.ListOfProducts = mapper.Map<IList<Data.PRODUCT>, List<Models.Product>>(products);
        }

        private Product GetProductModel(int id)
        {
            Data.PRODUCT oneProduct = logic.GetOneProduct(id);
            return mapper.Map<Data.PRODUCT, Product>(oneProduct);
        }

        // GET: Products
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("ProductIndex", model);
        }

        // GET: Products/Details/5
        public ActionResult Details(int id)
        {
            return View("ProductDetails", GetProductModel(id));
        }

        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete FAIL";
            if (logic.ProductDelete(id)) TempData["editResult"] = "Delete OK";
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            model.EditedProduct = GetProductModel(id);
            return View("ProductIndex", model);
        }

        [HttpPost]
        public ActionResult Edit(Product product, string editAction)
        {
            if (ModelState.IsValid && product != null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    logic.ProductCreate(product.Id, product.Name, product.Category, product.Size, product.Weight, product.Price);
                }
                else
                {
                    if (!logic.ProductModify(product.Id, product.Name, product.Category, product.Size, product.Weight, product.Price))
                    {
                        TempData["editResult"] = "Edit FAIL";
                    }
                }
                return RedirectToAction("Index");
            }
            else
            {
                ViewData["editAction"] = "Edit";
                model.EditedProduct = product;
                return View("ProductIndex", model);
            }
        }
    }
}
