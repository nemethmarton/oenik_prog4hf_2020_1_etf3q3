﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UnixStock.Web.Models;

namespace UnixStock.Web.Controllers
{
    public class ProductsApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        Logic.Logic logic;
        IMapper mapper;

        public ProductsApiController()
        {
            logic = new Logic.Logic();
            mapper = MapperFactory.CreateMapper();
        }

        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Product> GetAll()
        {
            var products = logic.GetAllProducts();
            return mapper.Map<IList<Data.PRODUCT>, List<Models.Product>>(products);
        }

        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneProduct(int id)
        {
            bool success = logic.ProductDelete(id);
            return new ApiResult() { OperationResult = success };
        }

        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneProduct(Product product)
        {
            logic.ProductCreate(product.Id, product.Name, product.Category, product.Size, product.Weight, product.Price);
            return new ApiResult() { OperationResult = true };
        }

        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneProduct(Product product)
        {
            bool success = logic.ProductModify(product.Id, product.Name, product.Category, product.Size, product.Weight, product.Price);
            return new ApiResult() { OperationResult = success };
        }
    }
}
