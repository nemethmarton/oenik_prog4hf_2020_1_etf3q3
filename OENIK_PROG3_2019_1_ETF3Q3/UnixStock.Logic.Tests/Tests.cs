﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using UnixStock.Logic;
using UnixStock.Repository;
using UnixStock.Data;

namespace UnixStock.Logic.Tests
{
    [TestFixture]
    public class Tests
    {
        Logic BL;
        Mock<IRepository<CLIENT>> mockedClientRepo;
        Mock<IRepository<PRODUCT>> mockedProductRepo;
        Mock<IRepository<DEPT>> mockedDeptRepo;
        Mock<IRepository<SALE>> mockedSaleRepo;
        Mock<IRepository<SALE_HAS_PRODUCT>> mockedShpRepo;
        Mock<IRepository<STOCK>> mockedStockRepo;

        [SetUp]
        public void Init()
        {
            mockedClientRepo = new Mock<IRepository<CLIENT>>();
            mockedProductRepo = new Mock<IRepository<PRODUCT>>();
            mockedDeptRepo = new Mock<IRepository<DEPT>>();
            mockedSaleRepo = new Mock<IRepository<SALE>>();
            mockedShpRepo = new Mock<IRepository<SALE_HAS_PRODUCT>>();
            mockedStockRepo = new Mock<IRepository<STOCK>>();

            List<CLIENT> clientMocks = new List<CLIENT>();
            clientMocks.Add(new CLIENT()
            { CLIENTID = 1, NAME = "A Béla", CATEGORY = "Private", SHIPPING_ADDRESS = "Fő utca 1.", DISCOUNT = 10 });
            clientMocks.Add(new CLIENT()
            { CLIENTID = 2, NAME = "B József", CATEGORY = "Private", SHIPPING_ADDRESS = "Kossuth utca 1.", DISCOUNT = 15 });
            clientMocks.Add(new CLIENT()
            { CLIENTID = 3, NAME = "OE Kft.", CATEGORY = "Company", SHIPPING_ADDRESS = "Bécsi út 96.", DISCOUNT = 49 });
            clientMocks.Add(new CLIENT()
            { CLIENTID = 4, NAME = "Óbudai Lajos", CATEGORY = "Private", SHIPPING_ADDRESS = "Bécsi út 96.", DISCOUNT = 49 });

            List<PRODUCT> productMocks = new List<PRODUCT>();
            productMocks.Add(new PRODUCT()
            { PRODUCTID = 1, NAME = "A product", PRICE = 10});
            productMocks.Add(new PRODUCT()
            { PRODUCTID = 2, NAME = "B product", PRICE = 100 });
            productMocks.Add(new PRODUCT()
            { PRODUCTID = 3, NAME = "C product", PRICE = 1000 });

            List<DEPT> deptsMocks = new List<DEPT>();
            deptsMocks.Add(new DEPT()
            { DEPTID = 1, ADDRESS = "Szentendrei út 100.", PHONE = 0690});

            List<SALE> saleMocks = new List<SALE>();
            saleMocks.Add(new SALE()
            { SALEID = 1, DEPTID = 1, CLIENTID = 1, SALEDATE = DateTime.Now });
            saleMocks.Add(new SALE()
            { SALEID = 2, DEPTID = 1, CLIENTID = 2, SALEDATE = DateTime.Now });
            saleMocks.Add(new SALE()
            { SALEID = 3, DEPTID = 1, CLIENTID = 3, SALEDATE = DateTime.Now });

            List<SALE_HAS_PRODUCT> shpMocks = new List<SALE_HAS_PRODUCT>();
            shpMocks.Add(new SALE_HAS_PRODUCT()
            { SALEID = 1, PRODUCTID = 1, QUANTITY = 2 });
            shpMocks.Add(new SALE_HAS_PRODUCT()
            { SALEID = 2, PRODUCTID = 3, QUANTITY = 1 });
            shpMocks.Add(new SALE_HAS_PRODUCT()
            { SALEID = 3, PRODUCTID = 4, QUANTITY = 3 });

            List<STOCK> stockMocks = new List<STOCK>();
            stockMocks.Add(new STOCK()
            { DEPTID = 1, PRODUCTID = 1, QUANTITY = 5 });
            stockMocks.Add(new STOCK()
            { DEPTID = 1, PRODUCTID = 2, QUANTITY = 5 });
            stockMocks.Add(new STOCK()
            { DEPTID = 1, PRODUCTID = 3, QUANTITY = 5 });

            mockedClientRepo.Setup(t => t.ReadAll()).Returns(clientMocks);
            mockedProductRepo.Setup(t => t.ReadAll()).Returns(productMocks);
            mockedDeptRepo.Setup(t => t.ReadAll()).Returns(deptsMocks);
            mockedSaleRepo.Setup(t => t.ReadAll()).Returns(saleMocks);
            mockedShpRepo.Setup(t => t.ReadAll()).Returns(shpMocks);
            mockedStockRepo.Setup(t => t.ReadAll()).Returns(stockMocks);

            BL = new Logic(mockedClientRepo.Object, mockedDeptRepo.Object, mockedProductRepo.Object, mockedSaleRepo.Object, mockedShpRepo.Object, mockedStockRepo.Object);
        }

        [Test]
        public void AddTest()
        {
            BL.ClientCreate(5, "Kovács Sándor");

            mockedClientRepo.Verify(v => v.Create(It.IsAny<CLIENT>()));
        }

        [Test]
        public void DeleteTest()
        {
            BL.ClientDelete(1);
            Assert.That(mockedClientRepo.Object.Read(1) == null);
        }

        [Test]
        public void ReadTest()
        {
            var result = BL.ClientList();
            Assert.That(result.StartsWith("A Béla"));
        }

        [Test]
        public void FakeClientTest()
        {
            var result = BL.FakeClients();
            Assert.That(result.Contains("OE Kft."));
        }

        [Test]
        public void JavaTest()
        {
            var result = BL.JavaEndpoint();
            Assert.That(result.StartsWith("GET"));
        }

        [Test]
        public void OverWriteTest()
        {
            BL.ClientCreate(5, "X");
            BL.ClientCreate(5, "Y");
            var list = mockedClientRepo.Object.ReadAll().ToList();
            foreach (var item in list)
            {
                if (item.NAME == "Y")
                {
                    Assert.That(true);
                }
            }
        }

        [Test]
        public void SaleTest()
        {
            foreach (var item in mockedSaleRepo.Object.ReadAll().ToList())
            {
                foreach (var item2 in mockedShpRepo.Object.ReadAll().ToList())
                {
                    if (item.SALEID == item2.SALEID)
                    {
                        Assert.That(true);
                    }
                }
            }
        }

        [Test]
        public void StockTest()
        {
            var item = mockedProductRepo.Object.ReadAll().ToList();
            var stock = mockedShpRepo.Object.ReadAll().ToList();
            Assert.That(stock.FirstOrDefault().PRODUCTID == item.FirstOrDefault().PRODUCTID);
        }

        [Test]
        public void PhoneTest()
        {
            foreach (var item in mockedDeptRepo.Object.ReadAll().ToList())
            {
                if (item.PHONE.ToString().StartsWith("06"))
                {
                    Assert.That(true);
                }
            }
        }

        [Test]
        public void DiscountTest()
        {
            foreach(var item in mockedClientRepo.Object.ReadAll())
            {
                Assert.That(item.DISCOUNT >= 10);
            }
        }
    }
}
